'''
author: Wei Shi at 2021.12.27.
version: 2021.12.5
function: plot the figure of electric energy level
'''
import matplotlib.pyplot as plt
import numpy as np 
from numpy import pi
from numpy import sqrt,exp
from scipy.special import erfc,erf
import mpl_toolkits.axisartist as axisartist

##########读取参数##############

def loadparam(filename):
    with open(filename,'r') as fp:
        #lines=fp.readlines()
        lines=fp.read().splitlines()
    para={}
    for line in lines:
        varstr=line.split('\t')
        key=varstr[0]
        if key in para:
            para[key].append ( varstr[1] )
        else:
            para[key] = [varstr[1]]
    return para

###########添加文本信息#########

def text_mid(position,name_for_ob, text_position):
    plt.text(0.2, position + text_position, name_for_ob, ha ='center', va ='center',fontsize = 6.5)
def text_left(position,name_for_ob , text_position):
    plt.text(-0.625, position + text_position, "%s"%name_for_ob, ha ='center', va ='center',fontsize = 6.5)
def text_mlx(position,name_for_ob, text_position):
    plt.text(0.5, position + text_position, name_for_ob, ha ='center', va ='center', fontsize=12)
def text_leftest(position, text_position , name_for_ob):
    plt.text(-1.25, position + text_position, "%s"%name_for_ob, ha ='center', va ='center',fontsize = 6.5)
def text_right(position,name_for_ob, text_position):
    plt.text(1.725, position + text_position, "%s"%name_for_ob, ha ='center', va ='center',fontsize = 6.5)
    
###########画轨道位置###########
    
def plot_leftest_hlines(position): #最左边
    plt.hlines(position ,xmin = -1.5,xmax = -1,color = 'blue')
def plot_left_hlines(position): #左边
    plt.hlines(position ,xmin = -0.75,xmax = -0.5,color = 'blue')
def plot_mid_hlines(position, colors = 'purple'): #中间
    plt.hlines(position ,xmin = 0,xmax = 1,color = colors)
def plot_right_hlines(position,colors = 'orange'): #最右
    plt.hlines(position ,xmin = 1.5,xmax = 2,color = colors)

#############连线###############
    
def connect_mid_to_right(position_for_mid ,position_for_right ):
    plt.plot([1.05,1.45],[position_for_mid,position_for_right ],linestyle='dashed',color = 'black',linewidth=0.75)
def connect_left_to_mid(position_for_left ,position_for_mid,colors = 'black' ): 
    plt.plot([-0.45,-0.05],[position_for_left, position_for_mid],linestyle='dashed',color = colors ,linewidth=0.75)
def connect_left_to_left(position_for_leftest,position_for_left):
    plt.plot([-0.95,-0.8],[position_for_leftest,position_for_left],linestyle='dashed',color = 'black',linewidth=0.75)

############填充电子############

def add_charge(x,y):
    plt.arrow(x , y - 0.25 , 0, 0.4,
              width=0.001,
              head_width=0.03,
              head_length=0.2,
             fc='black',
             ec='black')
    plt.arrow(x + 0.1, y + 0.25, 0, -0.4,
              width=0.001,
              head_width=0.03,
              head_length=0.2,
             fc='black',
             ec='black')

def add_odd_charge(x,y): 
    plt.arrow(x , y - 0.25 , 0, 0.5,
              width=0.001,
              head_width=0.03,
              head_length=0.2,
             fc='black',
             ec='black')

########非键轨道########################
def non_bonding(position ,ligand, position_all_of_d, position_all_of_p, position_of_all_mid,charge):

        position_for_pi = []
        num_of_non_bonding =  int(3*ligand + len(position_all_of_d) + len(position_all_of_p) + 1 - len(position_of_all_mid))
        #print(num_of_non_bonding)
        itera = 0.4
        if num_of_non_bonding % 3 == 0:
            x_itera = 0
            for i in range (3):
                itera_for_sepical = 0
                for i in range (int(num_of_non_bonding/3)):
                        position_of_ob = position + itera_for_sepical
                        plt.hlines(position_of_ob ,xmin = 0 + x_itera,xmax = 0.3 + x_itera,color = 'orange')
                        itera_for_sepical += itera
                        if charge:
                            plt.arrow(0.1 + x_itera, position_of_ob - 0.25 , 0, 0.4,width=0.001,head_width=0.03,head_length=0.2,fc='black',ec='black')
                            plt.arrow(0.2 + x_itera,  position_of_ob + 0.25, 0, -0.4,width=0.001,head_width=0.03,head_length=0.2,fc='black',ec='black')
                        itera_for_sepical += itera
                x_itera += 0.35
            plt.text(0.5, position_of_ob + 0.6, 'Non-bonding', ha ='center', va ='center',fontsize = 9)
                
        elif num_of_non_bonding % 2 == 0:
            x_itera = 0
            for i in range (2):
                itera_for_sepical = 0
                for i in range (int(num_of_non_bonding/2)):
                    position_of_ob = position + itera_for_sepical
                    plt.hlines(position_of_ob ,xmin = 0 + x_itera ,xmax = 0.45 + x_itera,color = 'orange')
                    itera_for_sepical += itera
                    if charge:
                        plt.arrow(0.2 + x_itera ,  position_of_ob - 0.25 , 0, 0.4,width=0.001,head_width=0.03,head_length=0.2,fc='black',ec='black')
                        plt.arrow(0.3 + x_itera ,  position_of_ob + 0.25 , 0, -0.4,width=0.001,head_width=0.03,head_length=0.2,fc='black',ec='black')
                x_itera += 0.55
            plt.text(0.5, position_of_ob + 0.6, 'Non-bonding', ha ='center', va ='center',fontsize = 9)
                    
        #添加轨道中心位置信息
        position_for_pi.append(position_of_ob - (itera/2)*(num_of_non_bonding/2-1))
        return position_for_pi,num_of_non_bonding
    
###############创建画图的类#############

class Plot_figure:
    '''
    Usage    - plot the figure of electric energy level
    Parameters -
            file_name : 文件名
            para_of_ob : s、p、d所包含的轨道组，及d轨道的劈裂系数
            ligand ：配体数
            ligand_for_np ：非局域配体数
            charge ：局域结构电子数
            charge_for_np ：非局域结构电子数
            list_of_s: s轨道信息
            list_of_p : p轨道信息
            list_of_d : d轨道信息
            mid_ob_num : 各分子轨道所包含的轨道条数
            single : 非成键轨道
            pi : 是否需要画局域轨道 pi = 1 局域 pi = 0 非局域
            mlx  : MLX名称
            mlx_for_np：非局域MLX名称
            field: 晶体场标记
            charge_for_mlx：MLX所带电荷
    '''
    def __init__( self, file_name = 'None' , para_of_ob = {} ,ligand = 0, ligand_for_np = 0, charge= 0 ,charge_for_np = 0, list_of_s = [],list_of_p =[],list_of_d =[], 
                 mid_ob_num = {} ,single ={} ,pi = None, mlx = '', mlx_for_np = 'None', field = '',   charge_for_mlx = 0):

        self._filename = file_name
        self._para_of_ob = para_of_ob

        self._position_for_pi = []

        self._charge = charge
        self._charge_for_mlx = charge_for_mlx
        self._list_of_s = list_of_s
        self._list_of_p = list_of_p
        self._list_of_d = list_of_d

        self._mid_ob_num = mid_ob_num

        self._ligand_for_np = ligand_for_np 

        self._ligand = ligand

        self._single = single

        self._position_of_ligand = 0
        self._charge_for_np = charge_for_np

        self._pi = pi

        self._mlx = mlx

        self._field = field
        self._num_of_non_bonding = 0

        self._position_of_left_s = {}
        self._position_of_left_p = {}
        self._position_of_left_d = {}
        self._position_for_mid = {}

        self._mlx_for_np = mlx_for_np
    
        self._text_position = 0.37
        self._initial_y = -15
        self._initial_y_left = 1
        self._initial_y_right = 1
        self._position_for_initial_d = 0
        self._position_for_initial_s = 0
        self._position_for_initial_p = 0

        #所有轨道位置
        self._position_all_of_d = []
        self._position_all_of_p = []

        

        #轨道中心位置
        self._position_for_p = 0
        self._position_for_d = 0
        self._position_for_special = {}
    
        self._position_for_pi = []
        self._position_of_all_mid =[]
        self._colors = [
            '#1f77b4',
            '#ff7f0e',
            '#2ca02c',
            '#d62728',
            '#9467bd',
            '#8c564b',
            '#e377c2',
            '#7f7f7f',
            '#bcbd22',
            '#17becf',
            ]
        #中间轨道颜色
        self._color_of_mid_ob = {}
        self._fig = plt.figure()
        self._ax = axisartist.Subplot(self._fig, 111)
        
    def show_and_save(self):
        if self._pi :
            plt.savefig(self._filename +'_'+ self._mlx + '.svg',format="svg")
            #plt.show()
            
        else:
            plt.savefig(self._filename +'_'+ self._mlx_for_np+ '.svg',format="svg")
            #plt.show()
            

    def set_ylimit(self):
        
        self._position_of_all_mid.sort()
        self._ax.set_ylim(self._position_of_all_mid[0] -1 ,self._position_of_all_mid[-1] +1)
        
    def plot(self):
        #定义价电子的y轴坐标
        #将绘图区对象添加到画布中
        self._fig.add_axes(self._ax)
        #通过set_axisline_style方法设置绘图区的底部及左侧坐标轴样式
        #"-|>"代表实心箭头："->"代表空心箭头
        self._ax.axis["bottom"].set_axisline_style("-|>", size = 1.5)
        self._ax.axis["left"].set_axisline_style("-|>", size = 1.5)
        #通过set_visible方法设置绘图区的顶部及右侧坐标轴隐藏
        self._ax.axis["top"].set_visible(False)
        self._ax.axis["right"].set_visible(False)
        #position_of_all_mid.sort()
        self._ax.set_xticks([])
        self._ax.set_yticks([])
        self._ax.set_ylabel('E(eV)')
        #ax.set_ylim(position_of_all_mid[0] -1 ,position_of_all_mid[-1] +1)
        self._ax.set_ylim(-5 ,15)
        self._ax.set_xlim(-2,2.5)


    def plot_leftest(self):
        
        #迭代系数，用来画同一组轨道之间的间距
        itera_for_leftest = 0
        
        #d轨道起始位置 
        self._position_for_initial_d = 1
        #画图
        for i in range (5):
            position_of_ob = self._position_for_initial_d + itera_for_leftest
            plot_leftest_hlines(position_of_ob)
            itera_for_leftest +=0.2
        text_leftest(position_of_ob , self._text_position , 'd')
        #d轨道中心位置
        self._position_for_d = position_of_ob - 0.25

        #s轨道起始位置
        self._position_for_initial_s = 5
        #画图
        plot_leftest_hlines(self._position_for_initial_s)
        text_leftest(self._position_for_initial_s, self._text_position , 's')


        itera_for_leftest = 0
        #p轨道起始位置
        self._position_for_initial_p = 10
        #画图
        for i in range (3):
            position_of_ob = self._position_for_initial_p + itera_for_leftest
            plot_leftest_hlines(position_of_ob)
            itera_for_leftest +=0.2
        text_leftest(position_of_ob, self._text_position , 'p')
        #p轨道中心位置
        self._position_for_p = position_of_ob - 0.15

    def plot_left(self):

        #画左边属于d的轨道
        for ob in self._list_of_d:
            #迭代系数，用来画同一组轨道之间的间距
            if self._field == 'tri3':
                if ob[0] == "e''":
                    itera = 0.1
                else:
                    itera = 0.3
            else:
                itera = 0.3
            itera_for_left = 0
            for i in range(int(ob[1])):
                # para_of_ob[ob[0]][0] 轨道劈裂系数
                if self._field == 'squ5':
                    co = 0.35
                elif self._field == 'fsqu4':
                    co = 0.25
                else:
                    co = 0.5
                position_of_ob = self._position_for_initial_d + co * float(self._para_of_ob[ob[0]][0]) + itera_for_left
                self._position_all_of_d.append(position_of_ob)
                plot_left_hlines(position = position_of_ob)
                itera_for_left += itera
            #确定该组轨道的中心位置
            self._position_of_left_d[ob[0]] = position_of_ob - ((itera/2) * (int(ob[1])-1))
            #文本信息
            if self._field == 'tri3':
                if ob[0] == "a1'":
                    text_left(position = position_of_ob, name_for_ob = ob[0], text_position= -2*self._text_position)
                else:
                    text_left(position = position_of_ob, name_for_ob = ob[0], text_position=self._text_position)
            elif self._field == 'fsqu4':
                if ob[0] == "eg":
                    text_left(position = position_of_ob, name_for_ob = ob[0], text_position= -2*self._text_position)
                else:
                    text_left(position = position_of_ob, name_for_ob = ob[0], text_position=self._text_position)
            else:
                text_left(position = position_of_ob, name_for_ob = ob[0], text_position=self._text_position)

        #画左边属于s轨道
        itera = 0.3
        position_all_of_s =[]
        for ob in self._list_of_s:
            for i in range(int(ob[1])):
                plot_left_hlines(position = self._position_for_initial_s)
            #确定该组轨道的中心位置
            self._position_of_left_s[ob[0]] = self._position_for_initial_s
            #文本信息
            text_left(position = self._position_for_initial_s, name_for_ob = ob[0], text_position=self._text_position)

        #画p轨道
        itera = 0.3
        position_all_of_p =[]
        itera_for_left = 0
        for ob in self._list_of_p:
            for i in range(int(ob[1])):
                position_of_ob = self._position_for_initial_p + itera_for_left
                self._position_all_of_p.append(position_of_ob)
                plot_left_hlines(position = position_of_ob)
                itera_for_left += itera
            #确定该组轨道的中心位置
            self._position_of_left_p[ob[0]] = position_of_ob - ((itera/2) * (int(ob[1])-1))
            #文本信息
            text_left(position = position_of_ob, name_for_ob = ob[0],text_position=self._text_position)
            #确定一个根属于p轨道的轨道位置
            itera_for_left += 0.5

    def plot_ligand(self):
        self._position_all_of_d.sort()
        #ligand轨道起始位置
        self._position_of_initial_sigma = self._position_all_of_d[0] - 4
        #确定ligand轨道条数
        #画ligand
        itera = 0.4
        itera_for_right = 0 
        
        if self._pi:
            num_of_sigma = 3*self._ligand
            for i in range (int(num_of_sigma)):
                position_of_ob = self._position_of_initial_sigma + itera_for_right
                plot_right_hlines(position_of_ob)
                itera_for_right += itera
            text_right(position_of_ob,'ligand', self._text_position)
            #sigma轨道中心位置
            self._position_of_ligand  = position_of_ob - (num_of_sigma - 1) * (itera/2)

        else:
            num_of_sigma = 3*self._ligand_for_np
            for i in range (int(num_of_sigma)):
                position_of_ob = self._position_of_initial_sigma + itera_for_right
                plot_right_hlines(position_of_ob)
                itera_for_right += itera
            text_right(position_of_ob,'ligand', self._text_position)
            #ligand轨道中心位置
            self._position_of_ligand = position_of_ob - (num_of_sigma - 1) * (itera/2)

    def plot_mid(self):
        
        index_of_mid_ob = 0
        itera_for_mid = 0
        itera = 0.4

        key_of_s = []
        key_of_p = []
        key_of_d = []
        
        for i in self._list_of_d:
            key_of_d.append (i[0])

        for i in self._list_of_p:
            key_of_p.append (i[0])

        for i in self._list_of_s:
            key_of_s.append (i[0])

        for ob in self._list_of_d:
            #print(ob[0])
            if ob[0] in self._single:
                continue
            elif ob[0] in key_of_s:
                continue
            else:                
                for i in range (int(ob[1])):
                    if self._field == 'tet4':
                        if ob[0] == 't2':
                            co = 2
                        else:
                            co = 0.4
                    elif self._field == 'fsqu4':
                        if ob[0] == 'b1g':
                            co = 1
                        elif ob[0] == 'eg':
                            co = 1.5
                        elif ob[0] == 'b2g':
                            co = 0.45
                        else:
                            co = 0.4
                    else:
                        co = 0.4
                    position_of_ob = self._position_of_initial_sigma - co * (self._position_of_left_d[ob[0]] - self._position_of_initial_sigma) + itera_for_mid
                    self._position_of_all_mid.append(position_of_ob)
                    #画轨道
                    plot_mid_hlines(position_of_ob, colors = self._colors[index_of_mid_ob] )
                    itera_for_mid += itera
                #文本信息
                text_mid(position_of_ob, ob[0], self._text_position)
                #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
                self._position_for_mid[ob[0]] = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))
                #添加轨道颜色信息
                self._color_of_mid_ob[ob[0]] = self._colors[index_of_mid_ob]
                index_of_mid_ob += 1

        itera_for_mid = 0
        for ob in self._list_of_s:
            for i in range (int(ob[1])):
                co = 1.1
                position_of_ob = self._position_of_initial_sigma - co  * (self._position_of_left_s[ob[0]] - self._position_of_initial_sigma) + itera_for_mid
                self._position_of_all_mid.append(position_of_ob)
                #画轨道
                plot_mid_hlines(position_of_ob, colors = self._colors[index_of_mid_ob] )
                itera_for_mid += itera
            #文本信息
            text_mid(position_of_ob, ob[0], self._text_position)
            #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
            self._position_for_mid[ob[0]] = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))
            #添加轨道颜色信息
            self._color_of_mid_ob[ob[0]] = self._colors[index_of_mid_ob]
            index_of_mid_ob += 1

        itera_for_mid = 0
        for ob in self._list_of_p:
            if ob[0] in self._single:
                    continue
            elif ob[0] in key_of_d:
                    continue
            else:
                for i in range (int(ob[1])):
                    if self._field == 'fsqu4':
                        if ob[0] == 'a2u':
                            co = 0.1
                        elif ob[0] == 'eu':
                            co = 0.3
                        else:
                            co = 0.6
                    else:                        
                        co = 0.6
                    position_of_ob = self._position_of_initial_sigma - co *(self._position_of_left_p[ob[0]] - self._position_of_initial_sigma) + itera_for_mid
                    self._position_of_all_mid.append(position_of_ob)
                    #画轨道
                    plot_mid_hlines(position_of_ob, colors = self._colors[index_of_mid_ob] )
                    itera_for_mid += itera
                #文本信息
                text_mid(position_of_ob, ob[0], self._text_position)
                #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
                self._position_for_mid[ob[0]] = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))
                #添加轨道颜色信息
                self._color_of_mid_ob[ob[0]] = self._colors[index_of_mid_ob]
                index_of_mid_ob += 1
        
        itera = 0.4
        itera_for_mid = 0
        for key in self._single:
            if self._field == 'oct6' and self._pi == 1:
                position_for_t2g =  self._position_all_of_d[0] - 0.3
                for key in self._single:
                    for i in range (self._single[key]):
                        position_of_ob = position_for_t2g + itera_for_mid -4 
                        #添加轨道起始位置
                        self._position_of_all_mid.append(position_of_ob)
                        #画轨道
                        plot_mid_hlines(position_of_ob, colors = self._colors[index_of_mid_ob])
                        itera_for_mid += itera
                    #文本信息
                    text_mid(position_of_ob, key, self._text_position)
                    #添加轨道中心位置信息 key = 非成键轨道名称 single[key] = 非成键轨道的条数
                    self._position_for_mid[key] = position_of_ob - ((itera/2) * (self._single[key]-1))
                    self._position_for_pi.append (position_of_ob - ((itera/2) * (self._single[key]-1)))
                    #添加轨道颜色信息
                    self._color_of_mid_ob[key] = self._colors[index_of_mid_ob]

                for key in self._single:
                    for i in range (self._single[key]):
                        position_of_ob = position_for_t2g + itera_for_mid + 3 
                        #添加轨道起始位置
                        self._position_of_all_mid.append(position_of_ob)
                        #画轨道
                        plot_mid_hlines(position_of_ob, colors = self._colors[index_of_mid_ob])
                        itera_for_mid += itera
                    #文本信息
                    text_mid(position_of_ob, key + '*', self._text_position)
                    #添加轨道中心位置信息 key = 非成键轨道名称 single[key] = 非成键轨道的条数
                    self._position_for_mid[key + '*'] = position_of_ob - ((itera/2) * (self._single[key]-1))
                    self._position_for_pi.append (position_of_ob - ((itera/2) * (self._single[key]-1)))
                    #添加轨道颜色信息
                    self._color_of_mid_ob[key] = self._colors[index_of_mid_ob]        
                
            else: 
                if key in self._position_of_left_d:
                    position_of_ob = self._position_of_left_d[key] - ((itera/2) * (self._single[key]-1))
                elif key in self._position_of_left_p:
                    position_of_ob = self._position_of_left_p[key] - ((itera/2) * (self._single[key]-1))
                else:
                    break
                for i in range (self._single[key]):
                    
                    #添加轨道起始位置
                    self._position_of_all_mid.append(position_of_ob + itera_for_mid)
                    #画轨道
                    plot_mid_hlines(position_of_ob + itera_for_mid , colors = self._colors[index_of_mid_ob])
                    itera_for_mid += itera
                #文本信息
                text_mid(position_of_ob + itera_for_mid - itera, key, self._text_position)
                #添加轨道中心位置信息 key = 非成键轨道名称 single[key] = 非成键轨道的条数
                self._position_for_mid[key] = position_of_ob + itera_for_mid -itera - ((itera/2) * (self._single[key]-1))
                #添加轨道颜色信息
                self._color_of_mid_ob[key] = self._colors[index_of_mid_ob]
                    
        itera_for_mid = 0
        for ob in self._list_of_d:
            if ob[0] in self._single:
                continue
            elif ob[0] in key_of_s:
                continue
            elif self._field == 'fsqu4' and ob[0] == 'eg':
                continue
            elif self._field == 'fsqu4' and ob[0] == 'b2g':
                continue
            else:
                for i in range (int(ob[1])):
                    if self._field == 'tri6':
                        if ob[0] == "e'":
                            co = 5
                        elif ob[0] == "e''":
                            co = 1.8
                        else:
                            co = 1.4
                    elif self._field == 'tri5':
                        if ob[0] == "e'":
                            co = 4
                        else:
                            co = 1.4
                    elif self._field == 'oct8':
                        if ob[0] == 't2g':
                            co = 2
                        elif ob[0] == 'eg':
                            co = 3
                        else:
                            co = 1.4
                    elif self._field == 'tri3':
                        if ob[0] == "e''":
                            co = 2.5
                        else:
                            co = 1.4
                    elif self._field == 'squ5':
                        if ob[0] == 'b1': 
                            co = 2
                        else:
                            co = 1.4
                    elif self._field == 'tet4':
                        if ob[0] == 't2':
                            co = 4.5
                        else:
                            co = 1.4
                    elif self._field == 'fsqu4':
                        if ob[0] == 'b1g':
                            co = 3
                        else:
                            co = 1.4
                    else:
                        co = 1.4
                    position_of_ob = self._position_of_initial_sigma + co * (self._position_of_left_d[ob[0]] - self._position_of_initial_sigma) + itera_for_mid
                    self._position_of_all_mid.append(position_of_ob)
                    #画轨道
                    plot_mid_hlines(position_of_ob, colors = self._color_of_mid_ob[ob[0]] )
                    itera_for_mid += itera
                #文本信息
                text_mid(position_of_ob, ob[0] + '*', self._text_position)
                #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
                self._position_for_mid[ob[0] + '*'] = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))

        itera_for_mid = 0
        for ob in self._list_of_s:
            for i in range (int(ob[1])):
                co = 2.8
                position_of_ob = self._position_of_initial_sigma + co * (self._position_of_left_s[ob[0]] - self._position_of_initial_sigma) 
                self._position_of_all_mid.append(position_of_ob)
                #画轨道
                plot_mid_hlines(position_of_ob, colors = self._color_of_mid_ob[ob[0]] )
                itera_for_mid += itera
            #文本信息
            text_mid(position_of_ob, ob[0] + '*', self._text_position)
            #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
            self._position_for_mid[ob[0] + '*'] = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))

        itera_for_mid = 0
        for ob in self._list_of_p:
            if ob[0] in self._single:
                continue
            elif ob[0] in key_of_d:
                continue
            elif self._field == 'fsqu4' and ob[0] == 'a2u':
                continue
            else:
                for i in range (int(ob[1])):
                    if self._field == 'fsqu4':
                        if ob[0] == 'eu': 
                            co = 1.5
                        else:
                            co = 1.4 
                    else:
                        co = 1.6
                    position_of_ob = self._position_of_initial_sigma + co * (self._position_of_left_p[ob[0]] - self._position_of_initial_sigma) + itera_for_mid
                    self._position_of_all_mid.append(position_of_ob)
                    #画轨道
                    plot_mid_hlines(position_of_ob, colors = self._color_of_mid_ob[ob[0]] )
                    itera_for_mid += itera
                #文本信息
                text_mid(position_of_ob, ob[0] + '*', self._text_position)
                #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
                self._position_for_mid[ob[0] + '*'] = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))


    def plot_non_bonding(self):
        if self._field == 'oct6' :
            self._position_for_pi,self._num_of_non_bonding = non_bonding (-1.5,self._ligand ,self._position_all_of_d, self._position_all_of_p, self._position_of_all_mid,self._charge)
        elif self._field == 'tri5':
            self._position_for_pi,self._num_of_non_bonding = non_bonding(7,self._ligand,self._position_all_of_d, self._position_all_of_p, self._position_of_all_mid,self._charge)
        elif self._field == 'tri6':
            self._position_for_pi,self._num_of_non_bonding = non_bonding(-1,self._ligand,self._position_all_of_d, self._position_all_of_p, self._position_of_all_mid,self._charge)
        elif self._field == 'oct8':
            self._position_for_pi,self._num_of_non_bonding = non_bonding(-1,self._ligand,self._position_all_of_d, self._position_all_of_p, self._position_of_all_mid,self._charge)
        elif self._field == 'tri3':
            self._position_for_pi,self._num_of_non_bonding = non_bonding(2.5,self._ligand,self._position_all_of_d, self._position_all_of_p, self._position_of_all_mid,self._charge)
        elif self._field == 'squ5':
            self._position_for_pi,self._num_of_non_bonding = non_bonding(4,self._ligand,self._position_all_of_d, self._position_all_of_p, self._position_of_all_mid,self._charge)
        elif self._field == 'tet4':
            self._position_for_pi,self._num_of_non_bonding = non_bonding(2,self._ligand,self._position_all_of_d, self._position_all_of_p, self._position_of_all_mid,self._charge)

    def plot_special(self):
        
        itera = 0.4
        #画左边属于d的轨道
        if self._field == 'tri6':
            for ob in self._list_of_d:
                if ob[0] in self._position_of_left_p:
                    #迭代系数，用来画同一组轨道之间的间距
                    itera_for_left = 0
                    for i in range(int(ob[1])):
                        # para_of_ob[ob[0]][0] 轨道劈裂系数
                        position_of_ob = (self._position_of_left_d[ob[0]] + self._position_of_left_p[ob[0]])/2  + itera_for_left
                        self._position_of_all_mid.append(position_of_ob)
                        plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                        itera_for_left += itera
                    #文本信息
                    text_mid(position_of_ob, ob[0] + '*', self._text_position)
                    #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
                    p_for_center = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))
                    if ob[0] in self._position_for_special:
                        self._position_for_special[ob[0]].append(p_for_center)
                    else:
                        self._position_for_special[ob[0]] = [p_for_center]
            for ob in self._list_of_d:
                if ob[0] in self._position_of_left_s:
                    #迭代系数，用来画同一组轨道之间的间距
                    itera_for_left = 0
                    for i in range(int(ob[1])):
                        # para_of_ob[ob[0]][0] 轨道劈裂系数
                        position_of_ob = (self._position_of_left_d[ob[0]] + self._position_of_left_s[ob[0]])/2  + itera_for_left
                        self._position_of_all_mid.append(position_of_ob)
                        plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                        itera_for_left += itera
                    #文本信息
                    text_mid(position_of_ob, ob[0] + '*', self._text_position)
                    #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
                    p_for_center = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))
                    if ob[0] in self._position_for_special:
                        self._position_for_special[ob[0]].append(p_for_center)
                    else:
                        self._position_for_special[ob[0]] = [p_for_center]
                    

        elif self._field == 'tri5':
            for ob in self._list_of_d:
                if ob[0] in self._position_of_left_p:
                    itera_for_left = 0
                    for i in range(int(ob[1])):
                        position_of_ob = (self._position_of_left_d[ob[0]] + self._position_of_left_p[ob[0]])/2  + itera_for_left
                        self._position_of_all_mid.append(position_of_ob)
                        plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                        itera_for_left += itera
                    #文本信息
                    text_mid(position_of_ob, ob[0] + '*', self._text_position)
                    #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
                    p_for_center = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))
                    if ob[0] in self._position_for_special:
                        self._position_for_special[ob[0]].append(p_for_center)
                    else:
                        self._position_for_special[ob[0]] = [p_for_center]

            for ob in self._list_of_s:
                if ob[0] == "a1'":
                    position_of_ob = 10
                    self._position_of_all_mid.append(position_of_ob)
                    plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                    text_mid(position_of_ob, ob[0] + '*', self._text_position)

                    p_for_center = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))
                    if ob[0] in self._position_for_special:
                        self._position_for_special[ob[0]].append(p_for_center)
                    else:
                        self._position_for_special[ob[0]] = [p_for_center]

                    position_of_ob = -4
                    self._position_of_all_mid.append(position_of_ob)
                    plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                    text_mid(position_of_ob, ob[0] , self._text_position)
                    
                    p_for_center = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))
                    if ob[0] in self._position_for_special:
                        self._position_for_special[ob[0]].append(p_for_center)
                    else:
                        self._position_for_special[ob[0]] = [p_for_center]
                    
        elif self._field == 'oct8':
            # 孤立轨道a2u
            position_of_ob = -7  
            self._position_of_all_mid.append(position_of_ob)
            plot_mid_hlines(position = position_of_ob, colors = self._colors[-6])
            #文本信息
            text_mid(position_of_ob, 'a2u', self._text_position)

        elif self._field == 'tri3':
            for ob in self._list_of_d:
                if ob[0] == "a1'":
                    position_of_ob = self._position_of_left_d["a1'"] + 1
                    self._position_of_all_mid.append(position_of_ob)
                    plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                    #文本信息
                    text_mid(position_of_ob, ob[0] + '*', self._text_position)
                    #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数

                    p_for_center = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))
                    if ob[0] in self._position_for_special:
                        self._position_for_special[ob[0]].append(p_for_center)
                    else:
                        self._position_for_special[ob[0]] = [p_for_center]
                    
        elif self._field == 'squ5':
            for ob in self._list_of_p:
                if ob[0] == "a1":
                    position_of_ob = 17
                    plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                    self._position_of_all_mid.append(position_of_ob)
                    #文本信息
                    text_mid(position_of_ob, ob[0] + '*', self._text_position)
                    if ob[0] in self._position_for_special:
                       self._position_for_special[ob[0]].append(position_of_ob)
                    else:
                        self._position_for_special[ob[0]] = [position_of_ob]

                    position_of_ob = -3
                    plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                    self._position_of_all_mid.append(position_of_ob)
                    #文本信息
                    text_mid(position_of_ob, ob[0] + '*', self._text_position)
                    if ob[0] in self._position_for_special:
                        self._position_for_special[ob[0]].append(position_of_ob)
                    else:
                        self._position_for_special[ob[0]] = [position_of_ob]
                    
                    position_of_ob = -13
                    plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                    self._position_of_all_mid.append(position_of_ob)
                    #文本信息
                    text_mid(position_of_ob, ob[0] , self._text_position)
                    #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
                    if ob[0] in self._position_for_special:
                        self._position_for_special[ob[0]].append(position_of_ob)
                    else:
                        self._position_for_special[ob[0]] = [position_of_ob]

                elif ob[0] == 'e':
                    itera_for_left = 0
                    for i in range (int(ob[1])):
                        position_of_ob = self._position_of_left_p[ob[0]] - 0.15 + itera_for_left
                        plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                        self._position_of_all_mid.append(position_of_ob)
                        itera_for_left += itera
                    text_mid(position_of_ob, ob[0] + '*', self._text_position)
                    self._position_for_special[ob[0]] = [position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))]

        elif self._field == 'tet4':
            itera_for_left = 0
            for i in range (3):
                position_of_ob = (self._position_of_left_p['t2'] + self._position_of_left_d['t2'])/2 + itera_for_left
                plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob['t2'])
                self._position_of_all_mid.append(position_of_ob)
                itera_for_left += itera
            text_mid(position_of_ob, 't2' + '*', self._text_position)
            self._position_for_special['t2'] = [position_of_ob - ((itera/2) * (self._mid_ob_num['t2']-1))]

        elif self._field == 'fsqu4':
             for ob in self._list_of_d:
                if ob[0] in self._position_of_left_s:
                    #迭代系数，用来画同一组轨道之间的间距
                    itera_for_left = 0
                    for i in range(int(ob[1])):
                        # para_of_ob[ob[0]][0] 轨道劈裂系数
                        position_of_ob = (self._position_of_left_d[ob[0]] + self._position_of_left_s[ob[0]])/2  + itera_for_left
                        self._position_of_all_mid.append(position_of_ob)
                        plot_mid_hlines(position = position_of_ob, colors = self._color_of_mid_ob[ob[0]])
                        itera_for_left += itera
                    #文本信息
                    text_mid(position_of_ob, ob[0] + '*', self._text_position)
                    #添加轨道中心位置信息 ob[0] = 成键轨道名称 self._mid_ob_num[ob[0]] = 该轨道的条数
                    p_for_center = position_of_ob - ((itera/2) * (self._mid_ob_num[ob[0]]-1))
                    if ob[0] in self._position_for_special:
                        self._position_for_special[ob[0]].append(p_for_center)
                    else:
                        self._position_for_special[ob[0]] = [p_for_center]
            
    def plot_s_for_mid (self):
            self._position_of_all_mid.sort()
            itera_for_mid = 0
            itera = 0.4
            ligand_for_np = int(self._ligand_for_np)
            ligand = int(self._ligand)
            if self._pi == 0 :
                lig_for_itera = ligand_for_np
            else:
                lig_for_itera = ligand
            position_for_intial_s_for_mid = self._position_of_all_mid[0] - 1
            for i in range (lig_for_itera):
                 position_of_ob = position_for_intial_s_for_mid - itera_for_mid
                 self._position_of_all_mid.append(position_of_ob)
                 #画中间s轨道
                 plot_mid_hlines(position_of_ob, colors = 'black' )
                 #画右侧s轨道
                 plot_right_hlines(position_of_ob, colors = 'black')
                 itera_for_mid += itera
            text_mid(position_for_intial_s_for_mid, 's', self._text_position)
            text_right(position_for_intial_s_for_mid, 's', self._text_position)
            connect_mid_to_right(position_for_intial_s_for_mid - ((itera/2) * (lig_for_itera - 1)), position_for_intial_s_for_mid - ((itera/2) * (lig_for_itera - 1)))

    def connect(self):
        
        #left to left
        for key in self._position_of_left_d:
            connect_left_to_left(self._position_for_d , self._position_of_left_d[key],)

        connect_left_to_left(self._position_for_initial_s, self._position_for_initial_s)

        for key in self._position_of_left_p:
            connect_left_to_left(self._position_for_p , self._position_of_left_p[key])

        #left to mid
        for key in self._position_of_left_p:
            if key in self._position_for_special:
                for pos in self._position_for_special[key]:
                    connect_left_to_mid(self._position_of_left_p[key] , pos ,colors = self._color_of_mid_ob [key])
    
            connect_left_to_mid(self._position_of_left_p[key] , self._position_for_mid[key] ,colors = self._color_of_mid_ob [key])

            if (key + '*') in self._position_for_mid :
                connect_left_to_mid(self._position_of_left_p[key] , self._position_for_mid[key + '*'], colors = self._color_of_mid_ob [key])
            
        for key in self._position_of_left_s:
            if key in self._position_for_special:
                for pos in self._position_for_special[key]:
                    connect_left_to_mid(self._position_of_left_s[key] , pos ,colors = self._color_of_mid_ob [key])
            if key in self._single:
                continue

            connect_left_to_mid(self._position_of_left_s[key] , self._position_for_mid[key] ,colors = self._color_of_mid_ob [key])
            connect_left_to_mid(self._position_of_left_s[key] , self._position_for_mid[key + '*'],colors = self._color_of_mid_ob [key])

        for key in self._position_of_left_d:
            if key in self._position_for_special:
                 for pos in self._position_for_special[key]:
                    connect_left_to_mid(self._position_of_left_d[key] , pos ,colors = self._color_of_mid_ob [key])
            connect_left_to_mid(self._position_of_left_d[key] , self._position_for_mid[key] ,colors = self._color_of_mid_ob [key])
            if self._pi:
                if key + '*' in self._position_for_mid:
                    connect_left_to_mid(self._position_of_left_d[key] , self._position_for_mid[key + '*'],colors = self._color_of_mid_ob [key])
            else:
                if key in self._single:
                    continue
                if key + '*' in self._position_for_mid:
                    connect_left_to_mid(self._position_of_left_d[key] , self._position_for_mid[key + '*'],colors = self._color_of_mid_ob [key])
                
        #mid to right
        for key in self._position_for_mid:
            if (self._pi):
                connect_mid_to_right (self._position_for_mid[key] ,self._position_of_ligand)
            else:
                if key in self._single:
                    continue
                else:
                    connect_mid_to_right (self._position_for_mid[key] ,self._position_of_ligand)
            
        
        #special to right
        for key in self._position_for_special:
            for pos in self._position_for_special[key]:
                connect_mid_to_right (pos ,self._position_of_ligand )
        
        #pi to ligand
        for i in self._position_for_pi:
            connect_mid_to_right (i ,self._position_of_ligand )


    def add_charge(self):
        #if self._charge :

        charge_plot = []
        self._position_of_all_mid.sort()

        if self._pi:
            charge = self._charge - self._num_of_non_bonding * 2
        else:
            charge = self._charge_for_np

        num_of_ocucpy = int(charge / 2)

        for i in range (num_of_ocucpy):
            add_charge(0.45 , self._position_of_all_mid [i])

        if charge % 2:
            add_odd_charge(0.45 , self._position_of_all_mid[num_of_ocucpy])

        if self._pi:
            plt.text(0.5, self._position_of_all_mid[-1] + 1.4 + self._text_position, "(" + self._mlx + ")" + '$^{%d^-}$'%self._charge_for_mlx , ha ='center', va ='center',fontsize = '12')
        else:
            plt.text(0.5, self._position_of_all_mid[-1] + 1.4 + self._text_position, self._mlx_for_np, ha ='center', va ='center',fontsize = '12')
